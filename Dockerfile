FROM ubuntu:18.04

EXPOSE 5333

RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN apt-get install -y python3.7 python3-pip

RUN ln -sfn /usr/bin/python3.7 /usr/bin/python3 && ln -sfn /usr/bin/python3 /usr/bin/python && ln -sfn /usr/bin/pip3 /usr/bin/pip

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . /app
WORKDIR /app

CMD ["/usr/local/bin/gunicorn", "src.main.app:app" , "-b", "0.0.0.0:5333", "--timeout", "3600", "--graceful-timeout", "3600", "--log-level", "debug", "--keep-alive", "3600"]