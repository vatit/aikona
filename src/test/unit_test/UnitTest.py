from unittest import TestCase
import logging
import os
from os.path import join
from PIL import Image
from fpdf import FPDF

from src.main.app import app
from src.main.service.asynchronous_document_processor.AsynchronousDocumentProcessor import AsynchronousDocumentProcessor
from src.main.service.field_extractor.FieldExtractor import FieldExtractor
from src.main.service.MainService import MainService
from src.main.service.converting_service.ConvertingService import ConvertingService
from src.main.service.database_service.DatabaseService import DatabaseService


class UnitTest(TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.field_extractor = FieldExtractor()
        self.asynchronous_processor = AsynchronousDocumentProcessor()
        self.main_service = MainService()
        self.converting_service = ConvertingService()
        self.database_service = DatabaseService()
        self.APP_ROOT = os.path.dirname(os.path.abspath(__file__))
        self.WORKING_FOLDER = 'pdfs'
        self.outPath = self.APP_ROOT + '/converted_images'
        self.outFile = self.outPath + "/{}.pdf"
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def test_aikona_vat_number(self):
        input_text = "Greggs - The Home of Fresh Baking Customer Care Team Greggs Plc Fernwood House Clayton Road Newcastle upon Tyne NE2 1TL Tel:08081 473447 (08081 GREGGS) Email getintouch@greggs.co.uk Web: www.greggs.co.uk VAT: GB659880474 Steven 08/01/2019 7:54:11 AM Customer No. 74 Transaction No. 110975 Till No. 3 Shop No. 2376 Shop Id S02376 Take Away 66105162 S* 1x Ctr B/Fast Saus 2.30 S* 1x Lge Latte 2.65 Sub Total 4.95 Promotional * Summary 1x CTR+COFFEE -1.95 Sub Total -1.95 Total 3.00 Card Payment 3.00 Change 0.00 Vat Rates S : 20% 74231135 * Thank you for your custom Greggs - The Home of Fresh Baking Customer Care Team Greggs Plc Fernwood House Clayton Road Newcastle upon Tyne NE2 1TL Tel:08081 473447 (08081 GREGGS) Email getintouch@greggs.co.uk Web: www.greggs.co.uk VAT: GB659880474 Steven 08/01/2019 7:54:11 AM Customer No. 74 Transaction No. 110975 Till No. 3 Shop No. 2376 Shop Id S02376 Take Away 66105162 S* 1x Ctr B/Fast Saus 2.30 S* 1x Lge Latte 2.65 Sub Total 4.95 Promotional * Summary 1x CTR+COFFEE -1.95 Sub Total -1.95 Total 3.00 Card Payment 3.00 Change 0.00 Vat Rates S : 20% 74231135 * Thank you for your custom"
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['GB659880474']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_1(self):
        input_text = "vat registration number: 123456789 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['123456789']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_2(self):
        input_text = "vat number 1234567 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['1234567']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_3(self):
        input_text = "v.a.t. no.:12345678945 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['12345678945']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_4(self):
        input_text = "v.a.t. registration no 12345678945 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['12345678945']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_5(self):
        input_text = "vat reg. no.: 123456789 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['123456789']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_6(self):
        input_text = "v.a.t. reg.: 12345678945 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['12345678945']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_7(self):
        input_text = "vat registration 123456789 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['123456789']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_8(self):
        input_text = "ust.-id.:123456789 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['123456789']
        assert vat_number == ground_vat_number

    def test_vat_nr_regex_9(self):
        input_text = "SE123456789123 "
        vat_number = self.field_extractor.get_vat_numbers(input_text)
        ground_vat_number = ['SE123456789123']
        assert vat_number == ground_vat_number

    def test_copy_regex(self):
        input_text = "not original copy"
        copy = self.field_extractor.get_copies(input_text)
        ground_copy = ['not original', 'copy']
        assert copy == ground_copy

    def test_rental_agreement(self):
        input_text = "rental lease rent agreement"
        rent = self.field_extractor.get_rental_agreements(input_text)
        ground_rent = ['rental lease', 'rent agreement']
        assert rent == ground_rent

    def test_deposit(self):
        input_text = "deposit "
        deposit = self.field_extractor.get_deposits(input_text)
        ground_deposit = ['deposit']
        assert deposit == ground_deposit

    def test_information_invoice(self):
        input_text = "INFORMATION INVOICE "
        invoice = self.field_extractor.get_information_invoices(input_text)
        ground_invoice = ['INFORMATION INVOICE']
        assert invoice == ground_invoice

    def test_interim_invoice(self):
        input_text = "Interim Invoice "
        interim = self.field_extractor.get_interim_invoices(input_text)
        ground_interim = ['Interim']
        assert interim == ground_interim

    def test_asynchronous_processor(self):
        plain_text, blocks, process_flag, response_text = self.asynchronous_processor.asynchronous_document_processing_analysis(
            document='d9391c77-38de-4d86-ae64-4e0b388bed03.jpg')
        assert response_text['JobStatus'] == 'SUCCEEDED'

    def test_converting_to_pdf(self):
        path = os.getcwd()
        image_path = path[:-14]
        image = image_path + 'test/resources/PERSONAL0000006A.jpg'
        if os.path.isdir(self.outPath) is False:
            os.makedirs(self.outPath)

        im = Image.open(image)
        width, height = im.size

        # convert pixel in mm with 1px=0.264583 mm
        width, height = float(width * 0.264583), float(height * 0.264583)

        # given we are working with A4 format size
        pdf_size = {'P': {'w': 210, 'h': 297}, 'L': {'w': 297, 'h': 210}}

        # get page orientation from image size
        orientation = 'P' if width < height else 'L'

        #  make sure image size is not greater than the pdf format size
        width = width if width < pdf_size[orientation]['w'] else pdf_size[orientation]['w']
        height = height if height < pdf_size[orientation]['h'] else pdf_size[orientation]['h']

        if width > height:
            orientation = 'L'
        else:
            orientation = 'P'

        pdf = FPDF(orientation=orientation, format='A4')

        file_name = image[image.rfind('/') + 1:-4]
        pdf.add_page()
        pdf.image(image, w=width, h=height)
        pdf_file_name = self.outFile.format(file_name)
        file_counter = 1

        while os.path.isdir(pdf_file_name) is True:
            pdf_file_name = self.outFile.format(join(file_name, "_{}".format(file_counter)))
            file_counter += 1
        ground_path = image_path + 'test/unit_test/converted_images/PERSONAL0000006A.pdf'
        pdf.output(name=pdf_file_name, dest="F")
        logging.info('Pdf conversion done')
        assert ground_path == pdf_file_name

    def test_delete_image(self):
        path = os.getcwd()
        image_path = path[:-14]
        delete_path = image_path + 'test/unit_test/converted_images/PERSONAL0000006A.pdf'
        self.converting_service.delete_document(delete_path)
        pass

    def test_ocr_data_db(self):
        dms_id = '7d899718-22e0-4aae-a0cd-dede2e403322'
        data = self.database_service.get_ocr_data(dms_id)
        assert data.shape[0] != 0
