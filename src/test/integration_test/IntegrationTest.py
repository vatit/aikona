from unittest import TestCase
import logging
import boto3
import os
import requests

from src.main.app import app
from src.main.service.database_service.DatabaseService import DatabaseService


class IntegrationTest(TestCase):

    def setUp(self):
        self.app = app.test_client()
        self.database_service = DatabaseService()
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def test_health_check(self):
        logging.info('Integration test for health-check')
        response = self.app.get('/health-check')
        assert response.status_code == 200

    def test_textract_connectivety(self):
        document = 'PERSONAL0000032A.jpg'
        s3_connection = boto3.resource('s3')
        bucket = os.environ['s3_bucket']
        s3_object = s3_connection.Object(bucket, document)
        s3_response = s3_object.get()
        assert s3_response['ResponseMetadata']['HTTPStatusCode'] == 200

    def test_database_connectivity(self):
        connection = self.database_service.set_up_connection()
        dsn = connection.dsn
        ground_dsn = 'user=postgres password=xxx dbname=aikona host=aikona-dev.cbstx4flp7pt.eu-west-1.rds.amazonaws.com port=5432'
        assert ground_dsn == dsn

    def test_infrrd_connectivity(self):
        pass

    def test_sqs(self):
        pass

    def test_sns(self):
        pass

    def test_dms(self):
        dms_id = '7d899718-22e0-4aae-a0cd-dede2e403322'
        url = os.environ['dms_url'] + dms_id
        response = requests.get(url)
        assert response.status_code == 200
