NOT_ALLOWED_EXTENSIONS = ['.csv']
ALLOWED_EXTENSIONS = [".jpeg", ".jpg", ".png", ".pdf"]

INFRRD_ADDRESS_FIELDS = ["remitToAddress", "shipToAddress", "storeDetails"]
INFRRD_DOCUMENT_TYPE_RECEIPT = 'receipt'
INFRRD_DOCUMENT_TYPE_INVOICE = 'invoice'
INFRRD_NOT_ALLOWED_FIELDS = ['personrequester']
