import json


class StringNormalizer(object):

    def __init__(self):
        pass

    @staticmethod
    def single_quote_replacement(text):
        normalized_text = text.replace("'", "`")
        return normalized_text

    @staticmethod
    def list_to_json(blocks_list):
        json_payload = json.dumps(blocks_list)
        return json_payload
