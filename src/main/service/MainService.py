import logging
import re
from typing import List, Dict
import json

from src.main.service.asynchronous_document_processor.AsynchronousDocumentProcessor import AsynchronousDocumentProcessor
from src.main.service.field_extractor.FieldExtractor import FieldExtractor
from src.main.service.document_service.DocumentService import DocumentService
from src.main.service.database_service.DatabaseService import DatabaseService
from src.main.service.json_package_processor.JsonPackageProcessor import JsonPackageProcessor
from src.main.service.string_normalizer.StringNormalizer import StringNormalizer
from src.main.DataClasses import ExtractedField
from src.main.service.converting_service.ConvertingService import ConvertingService
from src.main.service.infrrd.Process_Invoice import Infrrd


class MainService(object):

    def __init__(self):
        self.asynchronous_document_processor = AsynchronousDocumentProcessor()
        self.field_extractor = FieldExtractor()
        self.document_service = DocumentService()
        self.database_service = DatabaseService()
        self.json_package_processor = JsonPackageProcessor()
        self.string_normalizer = StringNormalizer()
        self.converting_service = ConvertingService()
        self.infrrd = Infrrd()
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def asynchronous_text_analysis(self, document):
        logging.info('Processing document asynchronously')
        plain_text, blocks, process_flag, response_text = self.asynchronous_document_processor.asynchronous_document_processing_analysis(
            document)
        logging.info('Done processing document asynchronously')
        return plain_text, blocks, process_flag, response_text

    def get_vat_numbers(self, plain_text, blocks):
        logging.info('Obtaining VAT numbers')
        vat_numbers = self.field_extractor.get_vat_numbers(plain_text)
        logging.info('Done obtaining VAT numbers')
        field_objects = []
        for vat_number in vat_numbers:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=vat_number,
                                                   block_type='LINE',
                                                   strip_spaces=True)

            for field_block in field_blocks:
                field_objects.append(self.__convert_block_to_field(field_block, 'vat_number', vat_number))

        return field_objects

    def insert_results(self,
                       document_id,
                       json_text,
                       plain_text,
                       supplier_vat_numbers=[],
                       supplier_names=[],
                       supplier_addresses=[],
                       iso_currencies=[],
                       locales=[],
                       currencies=[],
                       expense_types=[],
                       invoice_dates=[],
                       invoice_numbers=[],
                       invoice_amounts_exc_vat=[],
                       invoice_vat_amounts=[],
                       invoice_amounts_inc_vat=[],
                       invoice_line_items=[],
                       infrrd_fields=[]):

        categorised_infrrd_fields = self.__categorise_fields(infrrd_fields)

        supplier_names = self.__append_fields(categorised_infrrd_fields, ['merchantname'])
        supplier_addresses = self.__append_fields(categorised_infrrd_fields, ['storeDetails'])
        iso_currencies = self.__append_fields(categorised_infrrd_fields, ['currencyiso'])
        currencies = self.__append_fields(categorised_infrrd_fields, ['currency'])
        invoice_dates = self.__append_fields(categorised_infrrd_fields, ['invoiceDateiso'])
        invoice_amounts_inc_vat = self.__append_fields(categorised_infrrd_fields, ['totalbillamount'])
        invoice_vat_amounts = self.__append_fields(categorised_infrrd_fields, ['tax', 'vatAmt'])
        invoice_vat_rates = self.__append_fields(categorised_infrrd_fields, ['vatPercent'])
        invoice_numbers = self.__append_fields(categorised_infrrd_fields, ['invoiceno'])
        expense_types = self.__append_fields(categorised_infrrd_fields, ['category'])

        self.database_service.insert_ocr_results(
            document_id,
            json_text,
            plain_text,
            self.serialize_field_objects(supplier_vat_numbers),
            self.serialize_field_objects(supplier_names),
            self.serialize_field_objects(supplier_addresses),
            self.serialize_field_objects(iso_currencies),
            self.serialize_field_objects(locales),
            self.serialize_field_objects(currencies),
            self.serialize_field_objects(expense_types),
            self.serialize_field_objects(invoice_dates),
            self.serialize_field_objects(invoice_numbers),
            self.serialize_field_objects(invoice_amounts_exc_vat),
            self.serialize_field_objects(invoice_vat_amounts),
            self.serialize_field_objects(invoice_vat_rates),
            self.serialize_field_objects(invoice_amounts_inc_vat),
            json.dumps(invoice_line_items)
        )

    def __categorise_fields(self, extracted_fields: List[ExtractedField]) -> Dict:
        categorised_fields = {}
        for field in extracted_fields:
            if field.field_name in categorised_fields:
                categorised_fields[field.field_name].append(field)
            else:
                categorised_fields[field.field_name] = [field]

        return categorised_fields

    def __append_fields(self, categorised_fields: Dict, keys: List) -> List[ExtractedField]:
        l = []
        for key in keys:
            try:
                l += categorised_fields[key]
            except KeyError:
                logging.info("No {} in dictionary".format(key))
        return l

    def serialize_field_objects(self, field_objects: List[ExtractedField]):
        serialised_object_list = []
        for field_object in field_objects:
            serialised_object_list.append(field_object.to_dict())
        return json.dumps(serialised_object_list)

    def check_vat_number_match(self, blocks, vat_numbers):
        vat_number_confidence = self.json_package_processor.match_vat_number(blocks, vat_numbers)
        return vat_number_confidence

    def blocks_to_json(self, blocks_list):
        json_payload = self.string_normalizer.list_to_json(blocks_list)
        return json_payload

    def replace_single_quotes(self, blocks):
        for block in blocks:
            try:
                replaced_text = self.string_normalizer.single_quote_replacement(block['Text'])
                block['Text'] = replaced_text
            except KeyError:
                pass
        return blocks

    def __remove_white_spaces(self, input_text):
        return input_text.replace(' ', '')

    def __get_field_blocks(self, blocks, field_data, block_type, strip_spaces=False):
        field_blocks = []
        field_regex = re.compile(field_data)
        for block in blocks:
            if block['BlockType'] == block_type:
                if strip_spaces is True:
                    text = self.__remove_white_spaces(block['Text'])
                else:
                    text = block['Text']

                field_matches = field_regex.findall(text)
                if len(field_matches) != 0:
                    field_blocks.append(block)
        return field_blocks

    def __convert_block_to_field(self, block, field_name, data):
        return ExtractedField(

            field_name=field_name,
            data=data,
            confidence_level=block['Confidence'],
            ocr_text=block['Text'],
            bounding_box=str(block['Geometry']['BoundingBox'])
        )

    def check_ocr_text(self, document_name):
        logging.info('Determining if OCR text exists')
        data = self.database_service.get_ocr_data(document_name)
        return data

    def verify_pdf_conversion(self, response_text, downloaded_path):
        converted_flag = False
        if response_text['JobStatus'] == 'FAILED' and response_text['StatusMessage'] == 'IMAGE_SIZE_LIMIT_EXCEEDED':
            pdf_file_path = self.converting_service.convert_to_pdf([downloaded_path])
            file_name = pdf_file_path.split('/')[-1]
            converted_flag = True
            return converted_flag, pdf_file_path, file_name
        else:
            logging.info('PDF conversion not performed. Textract failed due to: %s',
                         str(response_text['StatusMessage']))
            return converted_flag, '', ''

    def download_files_from_bucket(self, file_name):
        path = self.document_service.download_file_s3(file_name)
        return path

    def post_infrrd_image(self, file_bytes) -> Dict:
        job_ids = {
            'receipt': self.infrrd.post_receipt_api(file_bytes),
            'invoice': self.infrrd.post_invoice_api(file_bytes)
        }

        return job_ids

    def get_infrrd_results(self, job_ids: Dict) -> (List[ExtractedField], List):
        invoice_extracted_fields = []
        invoice_line_items = []
        receipt_extracted_fields = []
        receipt_line_items = []

        try:
            invoice_extracted_fields, invoice_line_items = self.infrrd.get_invoice_fields(job_ids['invoice'])
        except KeyError:
            logging.warning("No infrrd invoice api job provided")

        try:
            receipt_extracted_fields, receipt_line_items = self.infrrd.get_receipt_fields(job_ids['receipt'])
        except KeyError:
            logging.warning("No infrrd receipt api job provided")

        merged_fields = self.__merge_extracted_field_lists(invoice_extracted_fields, receipt_extracted_fields)

        merged_lines = invoice_line_items

        return merged_fields, merged_lines

    @staticmethod
    def __merge_extracted_field_lists(list_a: List[ExtractedField], list_b: List[ExtractedField]):
        merged_list = list_a

        for item_b in list_b:
            found_flag = False
            for item_a in list_a:
                if item_a.field_name == item_b.field_name and item_a.data == item_b.data:
                    found_flag = True
                    item_a.confidence_level = max(item_a.confidence_level, item_b.confidence_level)
            if found_flag is False:
                merged_list.append(item_b)

        return merged_list

    def get_compliance_indicators(self, plain_text, blocks):
        compliance_indicator_objects = []
        rental_agreements = self.field_extractor.get_rental_agreements(plain_text)
        for rental_agreement in rental_agreements:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=rental_agreement,
                                                   block_type='LINE',
                                                   strip_spaces=False)

            for field_block in field_blocks:
                compliance_indicator_objects.append(
                    self.__convert_block_to_field(field_block, 'compliance_indicator', rental_agreement))

        deposits = self.field_extractor.get_deposits(plain_text)
        for deposit in deposits:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=deposit,
                                                   block_type='LINE',
                                                   strip_spaces=False)

            for field_block in field_blocks:
                compliance_indicator_objects.append(
                    self.__convert_block_to_field(field_block, 'compliance_indicator', deposit))

        copies = self.field_extractor.get_copies(plain_text)
        for copy in copies:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=copy,
                                                   block_type='LINE',
                                                   strip_spaces=False)

            for field_block in field_blocks:
                compliance_indicator_objects.append(
                    self.__convert_block_to_field(field_block, 'compliance_indicator', copy))

        information_invoices = self.field_extractor.get_information_invoices(plain_text)
        for information_invoice in information_invoices:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=information_invoice,
                                                   block_type='LINE',
                                                   strip_spaces=False)

            for field_block in field_blocks:
                compliance_indicator_objects.append(
                    self.__convert_block_to_field(field_block, 'compliance_indicator', information_invoice))

        interim_invoices = self.field_extractor.get_interim_invoices(plain_text)
        for interim_invoice in interim_invoices:
            field_blocks = self.__get_field_blocks(blocks=blocks,
                                                   field_data=interim_invoice,
                                                   block_type='LINE',
                                                   strip_spaces=False)

            for field_block in field_blocks:
                compliance_indicator_objects.append(
                    self.__convert_block_to_field(field_block, 'compliance_indicator', interim_invoice))

        return compliance_indicator_objects

    @staticmethod
    def supplier_vat_number_extraction(results_list):
        aik_number_list = []
        inf_number_list = []
        aik_confidence_list = []
        inf_confidence_list = []
        average_list = []
        total = 0
        results = results_list[0]
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'vat_number':
                        aik_number_string = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        aik_number_list.append(aik_number_string)
                        aik_confidence_list.append(confidence)
                except KeyError:
                    aik_number_list = []
                    aik_confidence_list = []
                try:
                    if extracted_field['field_name'] == 'vatNumber':
                        inf_number_string = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        inf_number_list.append(inf_number_string)
                        inf_confidence_list.append(confidence)
                except KeyError:
                    inf_number_list = []
                    inf_confidence_list = []
        except KeyError:
            aik_number_list = []
            inf_number_list = []
            aik_confidence_list = []
            inf_confidence_list = []

        if len(aik_number_list) != 0 and len(inf_number_list) != 0:
            vat_number_list = aik_number_list + inf_number_list
            confidence_list = aik_confidence_list + inf_confidence_list
        elif len(aik_number_list) != 0 and len(inf_number_list) == 0:
            vat_number_list = aik_number_list
            confidence_list = aik_confidence_list
        else:
            vat_number_list = inf_number_list
            confidence_list = inf_confidence_list

        if len(vat_number_list) == 1:
            vat_number_list = vat_number_list[0]
        elif len(vat_number_list) == 0:
            vat_number_list = ''
        else:
            vat_number_list = vat_number_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return vat_number_list, confidence_list, average

    @staticmethod
    def supplier_name_extraction(results_list):
        supplier_name_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'merchantname':
                        supplier_name = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        supplier_name_list.append(supplier_name)
                        confidence_list.append(confidence)
                except KeyError:
                    supplier_name_list = []
                    confidence_list = []
        except KeyError:
            supplier_name_list = []
            confidence_list = []

        if len(supplier_name_list) == 1:
            supplier_name_list = supplier_name_list[0]
        elif len(supplier_name_list) == 0:
            supplier_name_list = ''
        else:
            supplier_name_list = supplier_name_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return supplier_name_list, confidence_list, average

    @staticmethod
    def supplier_address_extraction(results_list):
        supplier_address_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'storeDetails':
                        supplier_address = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        supplier_address_list.append(supplier_address)
                        confidence_list.append(confidence)
                except KeyError:
                    supplier_address_list = []
                    confidence_list = []
        except KeyError:
            supplier_address_list = []
            confidence_list = []

        if len(supplier_address_list) == 1:
            supplier_address_list = supplier_address_list[0]
        elif len(supplier_address_list) == 0:
            supplier_address_list = ''
        else:
            supplier_address_list = supplier_address_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return supplier_address_list, confidence_list, average

    @staticmethod
    def supplier_expense_type_extraction(results_list):
        expense_type_list = []
        confidence_list = []
        results = results_list[0]
        total = 0
        average_list = []
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'category':
                        expense_type = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        expense_type_list.append(expense_type)
                        confidence_list.append(confidence)
                except KeyError:
                    expense_type_list = []
                    confidence_list = []
        except KeyError:
            expense_type_list = []
            confidence_list = []

        if len(expense_type_list) == 1:
            expense_type_list = expense_type_list[0]
        elif len(expense_type_list) == 0:
            expense_type_list = ''
        else:
            expense_type_list = expense_type_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return expense_type_list, confidence_list, average

    @staticmethod
    def invoice_date_extraction(results_list):
        invoice_date_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'invoiceDateiso':
                        invoice_date = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        invoice_date_list.append(invoice_date)
                        confidence_list.append(confidence)
                except KeyError:
                    invoice_date_list = []
                    confidence_list = []
        except KeyError:
            invoice_date_list = []
            confidence_list = []

        if len(invoice_date_list) == 1:
            invoice_date_list = invoice_date_list[0]
        elif len(invoice_date_list) == 0:
            invoice_date_list = ''
        else:
            invoice_date_list = invoice_date_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_date_list, confidence_list, average

    @staticmethod
    def invoice_number_extraction(results_list):
        invoice_number_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'invoiceno':
                        invoice_number = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        invoice_number_list.append(invoice_number)
                        confidence_list.append(confidence)
                except KeyError:
                    invoice_number_list = []
                    confidence_list = []
        except KeyError:
            invoice_number_list = []
            confidence_list = []

        if len(invoice_number_list) == 1:
            invoice_number_list = invoice_number_list[0]
        elif len(invoice_number_list) == 0:
            invoice_number_list = ''
        else:
            invoice_number_list = invoice_number_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_number_list, confidence_list, average

    @staticmethod
    def invoice_vat_extraction(results_list):
        invoice_vat_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'vatAmt':
                        invoice_vat = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        invoice_vat_list.append(invoice_vat)
                        confidence_list.append(confidence)
                except KeyError:
                    invoice_vat_list = []
                    confidence_list = []
        except KeyError:
            invoice_vat_list = []
            confidence_list = []

        if len(invoice_vat_list) == 1:
            invoice_vat_list = invoice_vat_list[0]
        elif len(invoice_vat_list) == 0:
            invoice_vat_list = ''
        else:
            invoice_vat_list = invoice_vat_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_vat_list, confidence_list, average

    @staticmethod
    def invoice_total_amount_extraction(results_list):
        invoice_total_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'totalbillamount':
                        invoice_total = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        invoice_total_list.append(invoice_total)
                        confidence_list.append(confidence)
                except KeyError:
                    invoice_total_list = []
                    confidence_list = []
        except KeyError:
            invoice_total_list = []
            confidence_list = []

        if len(invoice_total_list) == 1:
            invoice_total_list = invoice_total_list[0]
        elif len(invoice_total_list) == 0:
            invoice_total_list = ''
        else:
            invoice_total_list = invoice_total_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_total_list, confidence_list, average

    @staticmethod
    def invoice_line_items_extraction(results_list):
        results = results_list[0]
        confidence_list = []
        average_list = []
        total = 0
        try:
            line_items = results['line_items']
            invoice_line_items = line_items['line_items']
            try:
                confidences = line_items['confidence_level']
                for confidence in confidences:
                    confidence = confidence * 100
                    confidence = round(confidence, 2)
                    average_list.append(confidence)
                    confidence = str(confidence) + '%'
                    confidence_list.append(confidence)
            except KeyError:
                # Currently Infrrd is not returning the confidence level for line items. Remove 1.0 & 100.0% once its returning the confidence level
                confidence_list = ['100.0%']
                average_list = [1.0]
            invoice_line_items_list = invoice_line_items
        except KeyError:
            invoice_line_items_list = []

        if len(invoice_line_items_list) == 1:
            invoice_line_items_list = invoice_line_items_list[0]
        elif len(invoice_line_items_list) == 0:
            invoice_line_items_list = ''
            confidence_list = []
        else:
            invoice_line_items_list = invoice_line_items_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        # elif confidence_list[0] == '100.0%':
        #     confidence_list = ''
        # Remove once returning confidence level
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_line_items_list, confidence_list, average

    @staticmethod
    def invoice_currency_extraction(results_list):
        invoice_currency_list = []
        confidence_list = []
        results = results_list[0]
        average_list = []
        total = 0
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'currencyiso':
                        invoice_currency = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = confidence * 100
                        confidence = round(confidence, 2)
                        average_list.append(confidence)
                        confidence = str(confidence) + '%'
                        invoice_currency_list.append(invoice_currency)
                        confidence_list.append(confidence)
                except KeyError:
                    invoice_currency_list = []
                    confidence_list = []
        except KeyError:
            invoice_currency_list = []
            confidence_list = []

        if len(invoice_currency_list) == 1:
            invoice_currency_list = invoice_currency_list[0]
        elif len(invoice_currency_list) == 0:
            invoice_currency_list = ''
        else:
            invoice_currency_list = invoice_currency_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        if len(average_list) != 0:
            for item in average_list:
                total = total + item
            average = total / len(average_list)
        else:
            average = 0

        return invoice_currency_list, confidence_list, average

    @staticmethod
    def determine_compliance(results_list):
        comp_indicator_list = []
        confidence_list = []
        results = results_list[0]
        try:
            extracted_fields = results['extracted_fields']
            for extracted_field in extracted_fields:
                try:
                    if extracted_field['field_name'] == 'compliance_indicator':
                        comp_indicator = extracted_field['data']
                        confidence = extracted_field['confidence_level']
                        confidence = round(confidence, 2)
                        confidence = str(confidence) + '%'
                        comp_indicator_list.append(comp_indicator)
                        confidence_list.append(confidence)
                except KeyError:
                    comp_indicator_list = []
                    confidence_list = []
        except KeyError:
            comp_indicator_list = []
            confidence_list = []

        if len(comp_indicator_list) == 1:
            comp_indicator_list = comp_indicator_list[0]
        elif len(comp_indicator_list) == 0:
            comp_indicator_list = ''
        else:
            comp_indicator_list = comp_indicator_list

        if len(confidence_list) == 1:
            confidence_list = confidence_list[0]
        elif len(confidence_list) == 0:
            confidence_list = ''
        else:
            confidence_list = confidence_list

        return comp_indicator_list, confidence_list

    @staticmethod
    def information_score(vat_number_list, supplier_name_list, supplier_address_list, expense_type_list,
                          invoice_date_list, invoice_number_list, invoice_vat_list, invoice_total_list,
                          invoice_currency_list):

        score = 0

        # 1) Calculate VAT number score
        if len(str(vat_number_list)) != 0:
            score = score + 1
        else:
            score = score

        # 2) Calculate supplier name score
        if len(supplier_name_list) != 0:
            score = score + 1
        else:
            score = score

        # 3) Calculate supplier address score
        if len(supplier_address_list) != 0:
            score = score + 1
        else:
            score = score

        # 4) Calculate expense type score
        if len(expense_type_list) != 0:
            score = score + 1
        else:
            score = score

        # 5) Calculate invoice date score
        if len(invoice_date_list) != 0:
            score = score + 1
        else:
            score = score

        # 6) Calculate invoice number
        if len(str(invoice_number_list)) != 0:
            score = score + 1
        else:
            score = score

        # 7) Calculate invoice VAT amount
        if len(str(invoice_vat_list)) != 0:
            score = score + 1
        else:
            score = score

        # 8) Calculate invoice gross amount
        if len(str(invoice_total_list)) != 0:
            score = score + 1
        else:
            score = score

        # 9) Calculate invoice currency
        if len(invoice_currency_list) != 0:
            score = score + 1
        else:
            score = score

        percentage = (score / 9) * 100
        percentage = str(round(percentage, 2)) + '%'

        return percentage

    @staticmethod
    def information_average(vat_number_average, supplier_average, supplier_address_average, expense_type_average,
                            invoice_date_average, invoice_number_average, invoice_vat_average, invoice_total_average,
                            invoice_currency_average):
        n = 0
        total = 0

        if vat_number_average != 0:
            total = total + vat_number_average
            n = n + 1
        else:
            total = total

        if supplier_average != 0:
            total = total + supplier_average
            n = n + 1
        else:
            total = total

        if supplier_address_average != 0:
            total = total + supplier_address_average
            n = n + 1
        else:
            total = total

        if expense_type_average != 0:
            total = total + expense_type_average
            n = n + 1
        else:
            total = total

        if invoice_date_average != 0:
            total = total + invoice_date_average
            n = n + 1
        else:
            total = total

        if invoice_number_average != 0:
            total = total + invoice_number_average
            n = n + 1
        else:
            total = total

        if invoice_vat_average != 0:
            total = total + invoice_vat_average
            n = n + 1
        else:
            total = total

        if invoice_total_average != 0:
            total = total + invoice_total_average
            n = n + 1
        else:
            total = total

        if invoice_currency_average != 0:
            total = total + invoice_number_average
            n = n + 1
        else:
            total = total

        average = total / n
        score = str(round(average, 2)) + '%'
        return score

    @staticmethod
    def standardize_line_items(results_list):
        results = results_list[0]
        line_string_list = []
        try:
            line_items = results['line_items']
            invoice_line_items = line_items['line_items']
            for line_item in invoice_line_items:
                line_string = line_item['line_string']
                line_string_list.append(line_string)
        except KeyError:
            line_string_list = []
        return line_string_list

    @staticmethod
    def return_compliance(compliance_list, confidence_list):
        compliance_flag = True
        if len(compliance_list) == 0:
            compliance_list = '-'
            confidence_list = '-'
            return compliance_flag, compliance_list, confidence_list
        else:
            compliance_flag = False
            return compliance_flag, compliance_list, confidence_list
