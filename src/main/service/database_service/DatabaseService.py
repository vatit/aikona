import psycopg2
import logging
import os
import pandas.io.sql as sqlio
import pandas as pd

from src.main.service.string_normalizer.StringNormalizer import StringNormalizer


class DatabaseService(object):

    def __init__(self):
        self.connection = self.set_up_connection()
        self.string_normalizer = StringNormalizer()
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    @staticmethod
    def set_up_connection():
        logging.info('Setting up a connection to the database: %s', os.environ['pg_database'])
        connection = psycopg2.connect(user=os.environ['pg_user'], password=os.environ['pg_password'],
                                      host=os.environ['pg_host'], port=os.environ['pg_port'],
                                      database=os.environ['pg_database'])
        logging.info('Connected to DB: %s', os.environ['pg_database'])
        return connection

    def insert_ocr_results(self,
                           document_id,
                           json_text,
                           plain_text,
                           supplier_vat_numbers='',
                           supplier_names='',
                           supplier_addresses='',
                           iso_currencies='',
                           locales='',
                           currencies='',
                           expense_types='',
                           invoice_dates='',
                           invoice_numbers='',
                           invoice_amounts_exc_vat='',
                           invoice_vat_amounts='',
                           invoice_vat_rates='',
                           invoice_amounts_inc_vat='',
                           invoice_line_items=''):
        logging.info('Inserting into ocr_data table')

        query_string = """INSERT INTO public.ocr_data (document_id, json_text, plain_text, supplier_vat_numbers,
                                                            supplier_names, supplier_addresses, iso_currencies,
                                                            locales, currencies, expense_types, invoice_dates, 
                                                            invoice_numbers, invoice_amounts_exc_vat, 
                                                            invoice_vat_amounts, invoice_vat_rates, 
                                                            invoice_amounts_inc_vat,
                                                            invoice_line_items ) VALUES (
                                                            '{}', '{}', '{}', '{}',
                                                            '{}', '{}', '{}', '{}',
                                                            '{}', '{}', '{}', '{}',
                                                            '{}', '{}', '{}', '{}', '{}'
                                                            );""".format(
            document_id,
            self.string_normalizer.single_quote_replacement(self.string_normalizer.list_to_json(json_text)),
            self.string_normalizer.single_quote_replacement(plain_text),
            self.string_normalizer.single_quote_replacement(supplier_vat_numbers),
            self.string_normalizer.single_quote_replacement(supplier_names),
            self.string_normalizer.single_quote_replacement(supplier_addresses),
            self.string_normalizer.single_quote_replacement(iso_currencies),
            self.string_normalizer.single_quote_replacement(locales),
            self.string_normalizer.single_quote_replacement(currencies),
            self.string_normalizer.single_quote_replacement(expense_types),
            self.string_normalizer.single_quote_replacement(invoice_dates),
            self.string_normalizer.single_quote_replacement(invoice_numbers),
            self.string_normalizer.single_quote_replacement(invoice_amounts_exc_vat),
            self.string_normalizer.single_quote_replacement(invoice_vat_amounts),
            self.string_normalizer.single_quote_replacement(invoice_vat_rates),
            self.string_normalizer.single_quote_replacement(invoice_amounts_inc_vat),
            self.string_normalizer.single_quote_replacement(invoice_line_items)
        )
        try:
            cursor = self.connection.cursor()
            cursor.execute(query_string)
            self.connection.commit()
            cursor.close()
            logging.info('Table insert done')
        except Exception as e:
            self.connection.rollback()
            logging.info('Encountering problems with database connection: %s', e)
        pass

    def get_ocr_data(self, document_id):
        data = pd.DataFrame()
        logging.info('Searching for existing OCR text in table...')

        document_id = document_id + str('%')
        query_string = "SELECT document_id, json_text, plain_text FROM public.ocr_data WHERE document_id LIKE '{}';".format(
            document_id)
        try:
            cursor = self.connection.cursor()
            cursor.execute(query_string)
            data = sqlio.read_sql_query(query_string, self.connection, index_col=None)
            cursor.close()
            logging.info('Search done')
        except Exception as e:
            self.connection.rollback()
            logging.info('Encountering problems with database connection: %s', e)
        return data
