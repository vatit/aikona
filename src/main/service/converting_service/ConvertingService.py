import os
from os.path import join
import logging
from PIL import Image
from fpdf import FPDF


class ConvertingService:
    def __init__(self):
        self.APP_ROOT = os.path.dirname(os.path.abspath(__file__))
        self.WORKING_FOLDER = 'pdfs'
        self.outPath = self.APP_ROOT + '/converted_images'
        self.outFile = self.outPath + "/{}.pdf"

    def convert_to_pdf(self, files):
        logging.info('Converting image to pdf')
        pdf_file_name = ''
        if os.path.isdir(self.outPath) is False:
            os.makedirs(self.outPath)

        for image in files:
            im = Image.open(image)
            width, height = im.size

            # convert pixel in mm with 1px=0.264583 mm
            width, height = float(width * 0.264583), float(height * 0.264583)

            # given we are working with A4 format size
            pdf_size = {'P': {'w': 210, 'h': 297}, 'L': {'w': 297, 'h': 210}}

            # get page orientation from image size
            orientation = 'P' if width < height else 'L'

            #  make sure image size is not greater than the pdf format size
            width = width if width < pdf_size[orientation]['w'] else pdf_size[orientation]['w']
            height = height if height < pdf_size[orientation]['h'] else pdf_size[orientation]['h']

            if width > height:
                orientation = 'L'
            else:
                orientation = 'P'

            pdf = FPDF(orientation=orientation, format='A4')

            file_name = image[image.rfind('/') + 1:-4]
            pdf.add_page()
            pdf.image(image, w=width, h=height)
            pdf_file_name = self.outFile.format(file_name)
            file_counter = 1

            while os.path.isdir(pdf_file_name) == True:
                pdf_file_name = self.outFile.format(join(file_name, "_{}".format(file_counter)))
                file_counter += 1

            pdf.output(name=pdf_file_name, dest="F")
            logging.info('Pdf conversion done')
            try:
                self.delete_document(image)
            except Exception as e:
                logging.warning('Encountering a problem with file deletion: %s', e)
        return pdf_file_name

    def delete_document(self, path):
        logging.info('Deleting document: %s', str(path))
        if os.path.exists(path):
            os.remove(path)
        else:
            logging.info('Document does not exist: %s', str(path))
            pass
