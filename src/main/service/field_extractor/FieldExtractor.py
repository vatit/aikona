import re
import logging


class FieldExtractor(object):

    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def get_vat_numbers(self, input_text):
        logging.info('Searching for VAT numbers...')

        input_text = re.sub(r'(\d)\s+(\d)', r'\1\2', input_text)

        vat_number_regexes = []
        bracket_regexes = []

        vat_number_regexes.append(
            re.compile(
                'vat registration number:?\s*\d{7,12}\s|v.a.t.? registration number:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat number:?\s*\d{7,12}\s|v.a.t.? number:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat no.?:?\s*\d{7,12}\s|v.a.t.? no.?:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat registration no.?:?\s*\d{7,12}\s|v.a.t.? registration no.?:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat reg.?\s*no.?:?\s*\d{7,12}\s|v.a.t.? reg.?\s*no.?:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat reg.?:?\s*\d{7,12}\s|v.a.t.? reg.?:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'vat registration.?:?\s*\d{7,12}\s|v.a.t.? registration.?:?\s*\d{7,12}\s',
                re.IGNORECASE)
        )

        vat_number_regexes.append(
            re.compile(
                'ust.? idnr.?:?\s*\d{9}\s|ust.?-id-nr.?:?\s*d{9}\s|ust.?-id.?:?\s*\d{9}\s|ust.? id.? nr.?:?\s*\d{9}\s|ust.?-ident.?-nr.?:?\s*\d{9}\s|ust.? ident.? nr.?:?\s*\d{9}\s|ust.?-nr.?:?\s*d{9}\s|ust.? nr.?:?\s*d{9}\s|ustid.?:?\s*\d{9}\s|ust.? id.?:?\s*\d{9}\s|ustid.?:?\s*d{9}\s|ustnr.?:?\s*d{9}\s',
                re.IGNORECASE)
        )

        bracket_regexes.append(
            re.compile('\(([^)]+)', re.IGNORECASE)

        )

        vat_numbers = []
        bracket_strings = []

        for regex in bracket_regexes:
            bracket_strings += regex.findall(input_text)

        if len(bracket_strings) != 0:
            bracket_string = ' '.join(bracket_strings)
            input_text = input_text + bracket_string
        else:
            input_text = input_text

        for regex in vat_number_regexes:
            vat_numbers += regex.findall(input_text)

        stripped_vat_numbers = []
        for vat_number in vat_numbers:
            stripped_vat_numbers.append(self.__extract_vat_number_digits(vat_number))

        # ISO VAT Numbers
        iso_vat_number_regexes = re.compile(
            'ATU\s*\d{8}\s|DK\s*\d{8,10}\s|ES\s*\d{9}\s|BE\s*\d{10}\s|(?:GB|DE)\s*\d{9}\s|NL\s*\d{9}\s*B\s*\d{2}\s?|FI\s*\d{8}\s|FR\s*\d{2}\s*\d{9}\s|SE\s*\d{12}\s',
            re.IGNORECASE)

        iso_vat_numbers = iso_vat_number_regexes.findall(input_text)

        for vat_number in iso_vat_numbers:
            stripped_vat_numbers.append(self.__remove_white_spaces(vat_number))

        return self.__get_unique_vat_numbers(stripped_vat_numbers)

    def get_copies(self, input_text):
        logging.info('Searching for copied documents...')
        regex = re.compile(
            'copy|copied|not\s*original|proforma|(?:customer\s*copy)|non\s*original|copy\s*of\s*invoice|kopia',
            re.IGNORECASE)
        return regex.findall(input_text)

    def get_rental_agreements(self, input_text):
        logging.info('Searching for rental agreements...')
        regex = re.compile(
            'rent\s*agreement|rental\s*agreement|rental\s*lease|rent\s*lease',
            re.IGNORECASE)
        return regex.findall(input_text)

    def get_deposits(self, input_text):
        logging.info('Searching for deposits...')
        regex = re.compile(
            'deposit|deposits',
            re.IGNORECASE)
        return regex.findall(input_text)

    def get_information_invoices(self, input_text):
        logging.info('Searching for information invoices...')
        regex = re.compile(
            'information\s*only|information\s*invoice|information\s*invoices|information\s*copy|informations\s*ausdruck|information\s*ausdruck',
            re.IGNORECASE)
        return regex.findall(input_text)

    def get_interim_invoices(self, input_text):
        logging.info('Searching for interim invoices...')
        regex = re.compile(
            'interim|interim/s*invoice|interim/s*invoices',
            re.IGNORECASE)
        return regex.findall(input_text)

    def __remove_white_spaces(self, input_text):
        return input_text.replace(' ', '')

    def __extract_vat_number_digits(self, input_text):
        stripped_text = self.__remove_white_spaces(input_text)
        vat_number = ''.join([n for n in stripped_text if n.isdigit()])
        return vat_number

    def __get_unique_vat_numbers(self, vat_numbers):
        unique_set = set(vat_numbers)
        return list(unique_set)
