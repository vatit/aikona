import requests
import json
import logging
import time
import os

from typing import List
from src.main.DataClasses import ExtractedField, ReceiptLineItem, InvoiceLineItem
from src.main import Constants


class Infrrd(object):

    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    @staticmethod
    def post_receipt_api(file_bytes):
        logging.info("Uploading file to Infrrd receipt API")

        url = os.environ['INFRRD_RECEIPT_UPLOAD_URL']
        files = {'fileUploaded': file_bytes}

        headers = {
            'apikey': os.environ['INFRRD_RECEIPT_API_KEY'],
        }

        response = requests.post(url, files=files, headers=headers)

        job_id = response.text.replace('\"', '')

        logging.info("Infrrd response: {}".format(response.status_code))
        logging.info("Infrrd jobid: {}".format(response.text.replace('\"', '')))

        return job_id

    @staticmethod
    def post_invoice_api(file_bytes):
        logging.info("Uploading file to Infrrd invoice API")

        url = os.environ['INFRRD_INVOICE_UPLOAD_URL']
        files = {'fileUploaded': file_bytes}

        headers = {
            'apikey': os.environ['INFRRD_INVOICE_API_KEY'],
        }

        response = requests.post(url, files=files, headers=headers)

        job_id = response.text.replace('\"', '')

        logging.info("Infrrd response: {}".format(response.status_code))
        logging.info("Infrrd jobid: {}".format(response.text.replace('\"', '')))

        return job_id

    @staticmethod
    def __format_fields(doc_response) -> List[ExtractedField]:
        extracted_fields = []

        try:
            if doc_response['status'] == 'EXTRACTED':
                fields = doc_response['data']['fields']

                for field in fields:
                    if fields[field]['confidence'] > 0.0 and field not in Constants.INFRRD_NOT_ALLOWED_FIELDS:
                        if field in Constants.INFRRD_ADDRESS_FIELDS:
                            extracted_fields.append(
                                ExtractedField(
                                    field_name=field,
                                    data=fields[field]['value']['address'],
                                    confidence_level=fields[field]['confidence'],
                                    ocr_text='',
                                    bounding_box=''
                                )
                            )
                        else:

                            if type(fields[field]['value']) == list:
                                for value in fields[field]['value']:
                                    extracted_fields.append(
                                        ExtractedField(
                                            field_name=field,
                                            data=value,
                                            confidence_level=fields[field]['confidence'],
                                            ocr_text='',
                                            bounding_box=''
                                        )
                                    )

                            else:
                                extracted_fields.append(
                                    ExtractedField(
                                        field_name=field,
                                        data=fields[field]['value'],
                                        confidence_level=fields[field]['confidence'],
                                        ocr_text='',
                                        bounding_box=''
                                    )
                                )
        except KeyError:
            logging.warning('Infrrd processing error')
        return extracted_fields

    @staticmethod
    def __get_receipt_line_items(doc_response: dict):
        lines = []

        try:
            if doc_response['status'] == 'EXTRACTED':
                line_items = doc_response['data']['lineItems']

                for line in line_items:
                    lines.append(
                        ReceiptLineItem(
                            line_number=line['lineNumber'],
                            quantity=line['quantity'],
                            quantity_unit=line['quantityUnit'],
                            unit_price=line['unitPrice'],
                            product_name=line['productName'],
                            product_id=line['productId'],
                            final_price=line['finalPrice'],
                            confidence=line['confidence']
                        )
                    )
        except KeyError:
            logging.warning('Infrrd processing error')

        return lines

    @staticmethod
    def __get_invoice_line_items(doc_response: dict):
        lines = []

        try:
            if doc_response['status'] == 'EXTRACTED':
                line_items = doc_response['data']['lineItems']

                for line in line_items:
                    lines.append(
                        InvoiceLineItem(
                            line_number=line['Line Number'],
                            line_string=line['LineString']
                        )
                    )
        except KeyError:
            logging.warning('Infrrd processing error')
        return lines

    @staticmethod
    def __get_fields(url, api_key, scan_id):

        headers = {
            "apikey": api_key,
            "Content-Type": "application/x-www-form-urlencoded"
        }

        req_url = url + str(scan_id)

        retry_counter = 0

        doc_data = {}

        while retry_counter < 5:
            logging.info("Getting results from Infrrd url: {}".format(req_url))

            response = requests.get(
                url=req_url,
                headers=headers
            )

            if response.status_code != 404:

                logging.info("Infrrd response: {}".format(response.status_code))

                doc_data = json.loads(response.text)
                job_status = doc_data['status']

                logging.info("Infrrd job status: {}".format(job_status))

                if job_status == 'EXTRACTED':
                    break

                retry_counter += 1
                retry_timer = retry_counter ** 2
                logging.info("Retry infrrd fetch in : {}s".format(retry_timer))
                time.sleep(retry_timer)
            else:
                logging.info("Infrrd job not found")
                break

        return doc_data

    def __get_line_items(self, doc_response:dict, document_type: str):

        if document_type == Constants.INFRRD_DOCUMENT_TYPE_RECEIPT:
            lines = self.__get_receipt_line_items(doc_response)

            serialisable_lines = [line_item.to_dict() for line_item in lines]

            return {
                'document_type': document_type,
                'line_items': serialisable_lines
            }
        elif document_type == Constants.INFRRD_DOCUMENT_TYPE_INVOICE:
            lines = self.__get_invoice_line_items(doc_response)

            serialisable_lines = [line_item.to_dict() for line_item in lines]
            return {
                'document_type': document_type,
                'line_items': serialisable_lines
            }
        else:
            return {
                'document_type': 'Invalid Document Type',
                'line_items': []
            }

    def get_receipt_fields(self, scan_id) -> (List[ExtractedField], dict):

        doc_response = self.__get_fields(
            url=os.environ['INFRRD_RECEIPT_UPLOAD_URL'],
            api_key=os.environ['INFRRD_RECEIPT_API_KEY'],
            scan_id=scan_id)

        extracted_fields: List[ExtractedField] = []
        line_items = []

        extracted_fields = self.__format_fields(doc_response=doc_response)

        line_items = self.__get_line_items(doc_response, Constants.INFRRD_DOCUMENT_TYPE_RECEIPT)

        return extracted_fields, line_items

    def get_invoice_fields(self, scan_id) -> (List[ExtractedField], dict):

        doc_response = self.__get_fields(
            url=os.environ['INFRRD_INVOICE_UPLOAD_URL'],
            api_key=os.environ['INFRRD_INVOICE_API_KEY'],
            scan_id=scan_id)

        extracted_fields: List[ExtractedField] = []
        line_items = []

        extracted_fields = self.__format_fields(doc_response=doc_response)

        line_items = self.__get_line_items(doc_response, Constants.INFRRD_DOCUMENT_TYPE_INVOICE)

        return extracted_fields, line_items


