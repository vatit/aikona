import boto3
import requests
import logging
import os
import src.main.Constants as Constants
from src.main.service.infrrd.Process_Invoice import Infrrd
from src.main.service.converting_service.ConvertingService import ConvertingService


class DocumentService(object):

    def __init__(self):
        self.__session = boto3.session.Session(region_name=os.environ['textract_region'])
        self.s3 = self.__session.resource('s3')
        self.infrrd = Infrrd()
        self.converting_service = ConvertingService()
        self.APP_ROOT = os.path.dirname(os.path.abspath(__file__))
        self.outPath = self.APP_ROOT + '/converted_images'
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def upload_image_file(self, file):
        filenames = []
        document_uploaded_flags = []
        if file.filename != '':
            file.seek(0)
            self.send_object_to_S3(os.environ['s3_bucket'], file.filename, file.read())
            filenames.append(file.filename)
            document_uploaded_flags.append(True)
        else:
            logging.info('File not uploaded')
            filenames.append(None)
            document_uploaded_flags.append(False)
        return filenames, document_uploaded_flags

    @staticmethod
    def download_dms_file(dms_id):
        dms_url = os.environ['dms_url'] + str(dms_id)

        logging.info('Downloading DMS file: {}'.format(dms_url))
        response = requests.get(dms_url)

        if response.status_code == 404:
            logging.info('File {} not found in DMS'.format(dms_url))
            file_name = dms_id
            file_bytes = None
        else:
            logging.info('File {} found in DMS'.format(dms_url))

            try:
                file_name = response.headers['Content-Disposition'].split('=')[-1]
                file_bytes = response.content
            except KeyError:
                logging.warning('File name not found')
                file_name = dms_id
                file_bytes = None

        return file_name, file_bytes

    def upload_dms_files(self, file_name, file_bytes):

        if file_bytes is None:
            document_uploaded_flag = False
        else:
            logging.info('Putting image {} in S3 bucket'.format(file_name))
            self.send_object_to_S3(os.environ['s3_bucket'], file_name, file_bytes)
            logging.info('Uploaded image {} in S3 bucket with name {}'.format(file_name, file_name))
            document_uploaded_flag = True

        return document_uploaded_flag

    def send_object_to_S3(self, bucket_name, key, body):
        logging.info('Uploading to S3 bucket: {}'.format(bucket_name))
        self.s3.Bucket(bucket_name).put_object(Key=key, Body=body)

    def allowed_file(self, filename):
        return '.' in filename and \
               filename.rsplit('.', 1)[1].lower() in Constants.ALLOWED_EXTENSIONS

    def upload_pdf_conversions(self, path):
        file_name = path.split('/')[-1]
        with open(path, "rb") as file:
            self.send_object_to_S3(os.environ['s3_bucket'], file_name, file.read())
        self.converting_service.delete_document(path)

    def download_file_s3(self, key):
        bucket = self.s3.Bucket(os.environ['s3_bucket'])
        if os.path.isdir(self.outPath) is False:
            os.makedirs(self.outPath)
        download_file_path = self.outPath + '/' + str(key)
        bucket.download_file(key, download_file_path)
        logging.info('Downloading from S3 bucket {}'.format(key))
        return download_file_path
