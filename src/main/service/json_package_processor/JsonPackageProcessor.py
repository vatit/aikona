import logging
import re

from src.main.service.string_normalizer.StringNormalizer import StringNormalizer


class JsonPackageProcessor(object):

    def __init__(self):
        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
        self.string_normalizer = StringNormalizer()

    def text_field_extraction(self, blocks):
        logging.info('Extracting text from blocks...')
        text_list = []
        for block in blocks:
            try:
                text_list.append(block['Text'])
            except KeyError:
                pass
        plain_text = ' '.join(text_list)
        normalized_text = self.string_normalizer.single_quote_replacement(plain_text)
        return normalized_text

    def __remove_white_spaces(self, input_text):
        input_text = input_text.replace(' ', '')
        return input_text

    def match_vat_number(self, blocks, vat_number):
        logging.info('Checking for VAT number matches in blocks...')
        if vat_number != '':
            for block in blocks:
                try:
                    confidence = block['Confidence']
                    text = block['Text']
                    text_removed_white_spaces = self.__remove_white_spaces(text)
                    regex = re.compile(vat_number)
                    list_vat_numbers = regex.findall(text_removed_white_spaces)
                    if list_vat_numbers != []:
                        vat_number_match = list_vat_numbers[0]
                        if vat_number_match == vat_number:
                            return confidence
                        else:
                            return 0
                except KeyError:
                    pass
        else:
            return 0
