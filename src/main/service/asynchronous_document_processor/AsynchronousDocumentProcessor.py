import boto3
import json
import os
import logging
import sys
from botocore.errorfactory import ClientError
from time import sleep

from src.main.service.json_package_processor.JsonPackageProcessor import JsonPackageProcessor


class AsynchronousDocumentProcessor(object):

    def __init__(self):
        self.json_package_processor = JsonPackageProcessor()
        self.client = boto3.client('textract',
                                   region_name=os.environ['textract_region'],
                                   endpoint_url=os.environ['textract_endpoint'])
        self.sqs = boto3.client('sqs', region_name=os.environ['textract_region'],)
        self.role_arn = os.environ['role_arn']
        self.topic_arn = os.environ['topic_arn']
        self.queue_url = os.environ['sqs_url']
        self.job_id = ''
        self.job_found = True

        logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

    def __get_results_document_analysis(self, job_id):
        max_results = 1000
        pagination_token = None
        finished = False
        display_flag = False

        while finished == False:
            if pagination_token == None:
                response = self.client.get_document_analysis(JobId=job_id,
                                                             MaxResults=max_results)
            else:
                response = self.client.get_document_analysis(JobId=job_id,
                                                             MaxResults=max_results,
                                                             NextToken=pagination_token)
            if response['JobStatus'] == 'SUCCEEDED':
                blocks = response['Blocks']
                logging.info('Asynchronous: Analyzing document text')

                if display_flag is True:
                    for block in blocks:
                        logging.info('Type: %s', block['BlockType'])
                        if block['BlockType'] != 'PAGE':
                            logging.info('Detected: %s' + block['Text'])
                            logging.info('Confidence: %s percent', block['Confidence'])

                        logging.info('Id: %s', block['Id'])
                        if 'Relationships' in block:
                            logging.info('Relationships: %s', block['Relationships'])
                        logging.info('Bounding Box: %s', block['Geometry']['BoundingBox'])
                        logging.info('Polygon: %s', block['Geometry']['Polygon'])
                        logging.info('')

                        if 'NextToken' in response:
                            pagination_token = response['NextToken']
                        else:
                            finished = True
                else:
                    pass
            else:
                logging.info('Encountering problems: %s', str(response['StatusMessage']))
            return response

    def asynchronous_document_processing_analysis(self, document):
        self.job_found = False
        logging.info('Asynchronous: Getting text analysis for asynchronous method')

        response = self.textract_request(document)

        logging.info('Start Job Id: %s', response['JobId'])
        dot_line = 0
        while not self.job_found:
            sqs_response = self.sqs.receive_message(QueueUrl=self.queue_url, MessageAttributeNames=['ALL'],
                                                    MaxNumberOfMessages=10)

            if sqs_response:

                if 'Messages' not in sqs_response:
                    if dot_line < 20:
                        dot_line = dot_line + 1
                    else:
                        dot_line = 0
                    sys.stdout.flush()
                    continue

                for message in sqs_response['Messages']:
                    notification = json.loads(message['Body'])
                    text_message = json.loads(notification['Message'])
                    logging.info('%s', text_message['JobId'])
                    logging.info('%s', text_message['Status'])
                    if str(text_message['JobId']) == response['JobId']:
                        logging.info('Matching Job Found: %s', text_message['JobId'])
                        self.job_found = True
                        response_text = self.__get_results_document_analysis(text_message['JobId'])
                        if response_text['JobStatus'] == 'SUCCEEDED':
                            process_flag = True
                            self.sqs.delete_message(QueueUrl=self.queue_url,
                                                    ReceiptHandle=message['ReceiptHandle'])

                            blocks = response_text['Blocks']

                            plain_text = self.json_package_processor.text_field_extraction(blocks)
                            return plain_text, blocks, process_flag, response_text
                        else:
                            process_flag = False
                            plain_text = ''
                            blocks = ''
                            logging.info('Could not process %s: %s', document, response_text['StatusMessage'])
                            self.sqs.delete_message(QueueUrl=self.queue_url,
                                                    ReceiptHandle=message['ReceiptHandle'])
                            return plain_text, blocks, process_flag, response_text
                    else:
                        logging.info("Job didn't match: %s : %s",
                                     str(text_message['JobId']), str(response['JobId']))

    def textract_request(self, document):
        retry_counter = 0
        while retry_counter < 6:
            try:
                logging.info("Making request to AWS textract for doc: {}".format(document))
                return self.client.start_document_analysis(
                    DocumentLocation={'S3Object': {'Bucket': os.environ['s3_bucket'], 'Name': document}},
                    FeatureTypes=["TABLES", "FORMS"],
                    NotificationChannel={'RoleArn': self.role_arn, 'SNSTopicArn': self.topic_arn})
            except Exception as e:
                logging.warning('Encountering Textract problems: %s', e)
                retry_timer = 3 ** retry_counter
                sleep(retry_timer)
                retry_counter += 1
        logging.error("Too many error responses received from AWS Textract. Giving up...")
        return None
