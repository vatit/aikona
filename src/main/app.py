from flask import Flask, request
import os
import flask
import logging
from typing import List
import base64

from src.main.service.MainService import MainService
from src.main.service.document_service.DocumentService import DocumentService
from src.main.DataClasses import ExtractedField
from src.main.DataClasses import Document
from src.main.service.infrrd.Process_Invoice import Infrrd

logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

APP_ROOT = '/'.join(os.path.abspath(__file__).replace('/app.py', '').split('/')[:-2])

app = Flask(__name__)
app.config['APP_ROOT'] = APP_ROOT
app.config['UPLOAD_FOLDER'] = APP_ROOT + '/uploads'
logging.info('Application is listening...')

main_service = MainService()
document_service = DocumentService()
infrrd = Infrrd()


@app.route('/health-check', methods=['GET'])
def health_check():
    return 'Service up.'


@app.route('/')
def index():
    return flask.render_template('index.html')


@app.route('/results', methods=['POST'])
def get_result_for_front_end():
    uploaded_file = None
    try:
        if request.files['invoice'].filename == '':
            uploaded_file = None
        else:
            uploaded_file = request.files['invoice']
    except KeyError:
        logging.info('No file uploaded by front end form')

    dms_ids = ''
    try:
        dms_ids = request.form['DMS_IDs']
    except KeyError:
        logging.info('No DMS IDs supplied by front end form')

    parameters = {
        'uploaded_file': uploaded_file,
        'DMS_IDs': dms_ids
    }

    results, image = analyze_async_text(parameters)
    vat_number_list, vat_nr_confidence_list, vat_number_average = main_service.supplier_vat_number_extraction(results)
    supplier_name_list, supplier_confidence_list, supplier_average = main_service.supplier_name_extraction(results)
    supplier_address_list, supplier_address_confidence_list, supplier_address_average = main_service.supplier_address_extraction(
        results)
    expense_type_list, expense_type_confidence_list, expense_type_average = main_service.supplier_expense_type_extraction(
        results)
    invoice_date_list, invoice_date_confidence_list, invoice_date_average = main_service.invoice_date_extraction(
        results)
    invoice_number_list, invoice_number_confidence_list, invoice_number_average = main_service.invoice_number_extraction(
        results)
    invoice_vat_list, invoice_vat_confidence_list, invoice_vat_average = main_service.invoice_vat_extraction(results)
    invoice_total_list, invoice_total_confidence_list, invoice_total_average = main_service.invoice_total_amount_extraction(
        results)
    invoice_items_list, invoice_items_confidence_list, invoice_items_average = main_service.invoice_line_items_extraction(
        results)
    invoice_currency_list, invoice_currency_confidence_list, invoice_currency_average = main_service.invoice_currency_extraction(
        results)
    score = main_service.information_score(vat_number_list, supplier_name_list,
                                           supplier_address_list, expense_type_list, invoice_date_list,
                                           invoice_number_list, invoice_vat_list, invoice_total_list,
                                           invoice_currency_list)
    average = main_service.information_average(vat_number_average, supplier_average,
                                               supplier_address_average, expense_type_average,
                                               invoice_date_average, invoice_number_average,
                                               invoice_vat_average, invoice_total_average, invoice_currency_average)
    line_items = main_service.standardize_line_items(results)
    compliance_list, compliance_confidence_list = main_service.determine_compliance(results)
    compliance_flag, returned_compliance_list, returned_confidence_list = main_service.return_compliance(
        compliance_list, compliance_confidence_list)
    return flask.render_template('results.html', image=image, vat_number=vat_number_list,
                                 vat_nr_confidence=vat_nr_confidence_list, supplier_name=supplier_name_list,
                                 supplier_name_confidence=supplier_confidence_list,
                                 supplier_address=supplier_address_list,
                                 supplier_address_confidence=supplier_address_confidence_list,
                                 expense_type=expense_type_list, expense_type_confidence=expense_type_confidence_list,
                                 invoice_date=invoice_date_list,
                                 invoice_date_confidence=invoice_date_confidence_list,
                                 invoice_number=invoice_number_list,
                                 invoice_number_confidence=invoice_number_confidence_list, invoice_vat=invoice_vat_list,
                                 invoice_vat_confidence=invoice_vat_confidence_list, invoice_total=invoice_total_list,
                                 invoice_total_confidence=invoice_total_confidence_list,
                                 invoice_items=invoice_items_list,
                                 invoice_items_confidence=invoice_items_confidence_list,
                                 invoice_currency=invoice_currency_list,
                                 invoice_currency_confidence=invoice_currency_confidence_list, score=score,
                                 average=average, line_items=line_items, len=len(line_items),
                                 compliance_flag=compliance_flag, compliance_list=returned_compliance_list,
                                 compliance_confidence_list=returned_confidence_list)


@app.route('/process-document', methods=['POST'])
def get_results_for_direct_request():
    uploaded_file = None
    try:
        uploaded_file = request.files['invoice']
    except KeyError:
        logging.info('No file uploaded by direct request')

    dms_ids = ''
    try:
        dms_ids = request.form['DMS_IDs']
    except KeyError:
        logging.info('No DMS IDs supplied by direct request')

    parameters = {
        'uploaded_file': uploaded_file,
        'DMS_IDs': dms_ids
    }

    results, _ = analyze_async_text(parameters)

    return flask.jsonify(results)


def analyze_async_text(file_parameters: dict):
    document_names = []
    uploaded_flags = []
    existing_text_flags = []
    infrrd_ids = []

    if file_parameters['uploaded_file'] is not None:
        logging.info('Use uploaded file')
        file = file_parameters['uploaded_file']
        data = main_service.check_ocr_text(file.filename)
        if data.shape[0] is 0:
            logging.info('Document has not been processed before')
            document_names, uploaded_flags = document_service.upload_image_file(file)
            existing_text_flags.append(False)
        else:
            logging.info('Document was processed before')
            document_names = [file.filename]
            uploaded_flags = [True]
            existing_text_flags.append(True)
        file.seek(0)
        infrrd_ids.append(main_service.post_infrrd_image(file.read()))
        file.seek(0)
        file_bytes = file.read()
    else:
        if file_parameters['DMS_IDs'] != '':
            dms_ids = file_parameters['DMS_IDs']
            dms_ids = dms_ids.split(',')
            for dms_id in dms_ids:
                data = main_service.check_ocr_text(dms_id)
                if data.shape[0] is 0:
                    logging.info('Document has not been processed before')
                    document_name, document_bytes = document_service.download_dms_file(dms_id)
                    uploaded_flag = document_service.upload_dms_files(document_name, document_bytes)
                    existing_text_flag = False
                else:
                    logging.info('Document was processed before')
                    uploaded_flag = True
                    existing_text_flag = True
                    document_name, document_bytes = document_service.download_dms_file(dms_id)

                if document_bytes is not None:
                    infrrd_id = main_service.post_infrrd_image(document_bytes)
                    file_bytes = document_bytes
                else:
                    infrrd_id = None

                document_names.append(document_name)
                uploaded_flags.append(uploaded_flag)
                existing_text_flags.append(existing_text_flag)
                infrrd_ids.append(infrrd_id)
    results = []
    for document_name, uploaded_flag, existing_text_flag, infrrd_id in zip(document_names, uploaded_flags,
                                                                           existing_text_flags, infrrd_ids):
        if uploaded_flag is True and document_name is not None and existing_text_flag is False:

            logging.info('Getting text from Textract...')

            plain_text, blocks, process_flag, response_text = main_service.asynchronous_text_analysis(document_name)
            status = str(response_text['JobStatus'])

            if process_flag is True:
                text_source = 'textract'
                message = ''
                converted_pdf = False
            else:
                path = main_service.download_files_from_bucket(document_name)
                converted_flag, downloaded_path, file_name = main_service.verify_pdf_conversion(response_text, path)
                if converted_flag is True:
                    document_service.upload_pdf_conversions(downloaded_path)
                    plain_text, blocks, process_flag, response_text = main_service.asynchronous_text_analysis(file_name)
                    status = str(response_text['JobStatus'])
                    if process_flag is False:
                        doc_response = Document(
                            document_id=document_name,
                            text_source='textract',
                            status=status,
                            message=str(response_text['StatusMessage']),
                            extracted_fields=[],
                            converted_pdf=False
                        )
                        results.append(doc_response.to_dict())
                        continue
                    text_source = 'textract'
                    message = ''
                    converted_pdf = True
                else:
                    doc_response = Document(
                        document_id=document_name,
                        text_source='textract',
                        status=status,
                        message=str(response_text['StatusMessage']),
                        extracted_fields=[],
                        converted_pdf=False
                    )
                    results.append(doc_response.to_dict())
                    continue

        elif uploaded_flag is True and document_name is not None and existing_text_flag is True:
            logging.info('Returning text in database...')
            data = main_service.check_ocr_text(document_name)
            plain_text = data.tail(1)['plain_text'].values[0]
            blocks = data.tail(1)['json_text'].values[0]
            message = ''
            text_source = 'database'
            status = 'SUCCEEDED'
            converted_pdf = False
        else:
            if document_name is None:
                document_name = ''
            doc_response = Document(
                document_id=document_name,
                text_source='',
                status='FAILED',
                message='Document not found in DMS',
                extracted_fields=[],
                converted_pdf=False
            )
            results.append(doc_response.to_dict())
            continue

        extracted_fields: List[ExtractedField] = []

        infrrd_fields, infrrd_line_items = main_service.get_infrrd_results(infrrd_id)
        try:
            extracted_fields += [infrrd_field.to_json() for infrrd_field in infrrd_fields]
        except TypeError:
            pass
        vat_numbers = main_service.get_vat_numbers(plain_text, blocks)
        try:
            extracted_fields += [vat_number.to_json() for vat_number in vat_numbers]
        except TypeError:
            pass

        logging.info('Determining whether document is compliant...')
        compliance_indicators = main_service.get_compliance_indicators(plain_text, blocks)
        try:
            extracted_fields += [compliance_indicator.to_json() for compliance_indicator in compliance_indicators]
        except TypeError:
            pass
        logging.info('Done determining compliance')

        main_service.insert_results(
            document_id=document_name,
            json_text=blocks,
            plain_text=plain_text,
            supplier_vat_numbers=vat_numbers,
            infrrd_fields=infrrd_fields,
            invoice_line_items=infrrd_line_items
        )

        text_source = 'test'
        status = 'tets'
        message = 'test'
        converted_pdf = False
        file_bytes = None

        doc_response = Document(
            document_id=document_name,
            text_source=text_source,
            status=status,
            message=message,
            extracted_fields=extracted_fields,
            line_items=infrrd_line_items,
            converted_pdf=converted_pdf
        )
        results.append(doc_response.to_dict())

    try:
        image = base64.b64encode(file_bytes).decode()
    except NameError:
        image = None
        logging.warning("No image available to be returned")
    except TypeError:
        image = None
        logging.warning("No image available to be returned")

    logging.info('Task done...')
    return results, image


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', threaded=True, use_reloader=False, port=5333)
