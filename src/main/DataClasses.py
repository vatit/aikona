import json
from typing import List, Dict
from dataclasses import dataclass, field


@dataclass
class ExtractedField:
    field_name: str
    data: str
    confidence_level: float
    ocr_text: str
    bounding_box: str

    def to_json(self):
        json_dict = {
            'field_name': self.field_name,
            'data': self.data,
            'confidence_level': self.confidence_level,
            'ocr_text': self.ocr_text,
            'bounding_box': self.bounding_box
        }
        return json.dumps(json_dict)

    def to_dict(self):
        json_dict = {
            'field_name': self.field_name,
            'data': self.data,
            'confidence_level': self.confidence_level,
            'ocr_text': self.ocr_text,
            'bounding_box': self.bounding_box
        }
        return json_dict


@dataclass
class Document:
    document_id: str
    status: str = ''
    message: str = ''
    extracted_fields: List[ExtractedField] = field(default_factory=list)
    line_items: Dict = field(default_factory=dict)
    text_source: str = 'no text'
    converted_pdf: bool = False

    def to_dict(self):
        json_dict = {'document_id': self.document_id,
                     'text_source': self.text_source,
                     'status': self.status,
                     'message': self.message,
                     'extracted_fields': [json.loads(extracted_field) for extracted_field in self.extracted_fields],
                     'line_items': self.line_items,
                     'converted_pdf': self.converted_pdf}
        for extracted_field in json_dict['extracted_fields']:
            if extracted_field['bounding_box'] == '':
                bounding_box_string = '{}'
            else:
                bounding_box_string = extracted_field['bounding_box'].replace("\'", '"')
            extracted_field['bounding_box'] = json.loads(bounding_box_string)
        return json_dict


@dataclass
class ReceiptLineItem:
    line_number: int
    quantity: float
    quantity_unit: str
    unit_price: float
    product_name: str
    product_id: str
    final_price: float
    confidence: float

    def to_dict(self):
        d = {
            'line_number': self.line_number,
            'quantity': self.quantity,
            'quantity_unit': self.quantity_unit,
            'unit_price': self.quantity_unit,
            'product_name': self.product_name,
            'product_id': self.product_id,
            'final_price': self.final_price,
            'confidence': self.confidence
        }
        return d


@dataclass
class InvoiceLineItem:
    line_number: int
    line_string: str

    def to_dict(self):
        d = {
            'line_number': self.line_number,
            'line_string': self.line_string
        }
        return d

