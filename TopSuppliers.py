import pandas as pd
import pyodbc
import requests
import logging
import boto3


def set_up_connection(autocommit):
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    connection_string = 'DRIVER=FreeTDS;SERVER=172.16.9.2;PORT=1433;DATABASE=Dragon;UID=Dragonplatform_apps;PWD=Lv31BYDCUeJH1pk4;TDS_VERSION=8.0'
    connection = pyodbc.connect(connection_string, autocommit=autocommit)
    return connection


def fetch_data(supplier):
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    connection = set_up_connection(autocommit=True)
    query_string = "SELECT TOP 7 I.EntityCode#Supplier, D.DmsID  FROM VT_INVOICE I JOIN VT_Entity_SupportingDocument D ON I.invoiceURL = D.documentID WHERE I.EntityCode#Supplier IN ('{}') AND I.invoiceURL != '';".format(
        supplier)
    data = pd.read_sql(query_string, connection)
    logging.info('Fetching data')
    return data


def download_dms(dms_id):
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    session = boto3.session.Session(region_name='eu-west-1',aws_access_key_id='AKIA27LFMQZLBIYNS3WL',aws_secret_access_key='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')
    s3 = session.resource('s3')
    dms_url = 'http://dms.vatit.io/documents/1.0/document/'
    dms_url = dms_url + str(dms_id.lower())
    logging.info(dms_url)
    try:
        response = requests.get(dms_url)
        logging.info(response)
        try:
            logging.info('Uploading the documents to S3')
            file_name = response.headers['Content-Disposition'].split('=')[-1]
            s3.Bucket('tb-textract-ocr').put_object(Key=file_name, Body=response.content)
            logging.info('Upload done')
        except KeyError:
            logging.info('File not found')
    except Exception as e:
        logging.warning('{}'.format(e))
        pass


def get_top_supplier_images():
    logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)
    logging.info('Obtaining images')
    df = pd.read_excel(io='topSuppliers.xlsx')
    supplier_data = df['EntityCode#Supplier'].to_list()
    for supplier in supplier_data:
        data = fetch_data(supplier)
        dms_ids = data['DmsID'].to_list()
        for dms_id in dms_ids:
            download_dms(dms_id)


get_top_supplier_images()
