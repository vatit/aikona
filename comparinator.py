import requests
import json
import pandas as pd
import time

aikona_url = 'http://aikona.techbranch.io/process-document'
# df = pd.read_csv('/Users/clarissacoetzee/Desktop/dms_upload/sample_dms_ids.csv')
df = pd.read_csv('testing_dms_ids.csv', sep=';')
data = df.where(df['Status'] == "succeeded")['DmsID'].dropna()

klippa_url = 'https://custom-ocr.klippa.com/api/v1/parseDocument'
header = {'X-Auth-Key': 'gxR5Vaf7jcXaj1sxqm087jrxeagGFyxd'}

dms_url = 'https://dms.rkshop.io/documents/1.0/document/'

infrrd_improved_model_url = ''

fields = []

klippa_field_map = {
    'amount': 'invoice_total',
    'vatamount': 'invoice_vat_amount',
    'currency': 'invoice_currency',
    'date': 'invoice_date',
    'lines': 'line_items',
    'invoice_number': 'invoice_number',
    'receipt_number': 'invoice_number',
    'merchant_name': 'merchant_name',
    'merchant_vat_number': 'merchant_vat_number',
    'merchant_address': 'merchant_address',
    # vatitems has a list of vat rates and amounts and needs to be handled seperately
}

klippa_vat_item_field_map = {
    'amount': 'invoice_vat_amount',
    'percentage': 'invoice_vat_percentage'
}

infrrd_field_map = {
    'totalbillamount': 'invoice_total',
    'vatAmt': 'invoice_vat_amount',
    'tax': 'invoice_vat_amount',
    'currencyiso': 'invoice_currency',
    'billingDate': 'invoice_date',
    'invoiceDateiso': 'invoice_date',
    'vatPercent': 'invoice_vat_rate',
    'invoiceno': 'invoice_number',
    'merchantName': 'merchant_name',
    'vatnumber': 'merchant_vat_number',
    'StoreDetails': 'merchant_address'
}

c = 0
for dms_id in data:
    time.sleep(2)
    c += 1
    print('------------------')
    print(c)
    payload = {'DMS_IDs': dms_id}
    try:

        download_url = dms_url + str(dms_id)
        print('Starting with {}'.format(download_url))
        file = requests.get(url=download_url)
        print('Done with DMS, {}'.format(file.status_code))

        infrrd_api_key = "afa4c7fa-e52f-4c37-93e9-497869ddb997"

        url = "https://d.infrrdapis.com/ocr/v2/demo3/invoice/"
        files = {
            'fileUploaded': file.content,
        }

        parameters = {
            'extractionParameters': '{"orchestrator":true, "additionalFields":["vatid"]}'
        }

        headers = {
            'apikey': infrrd_api_key,
        }

        response = requests.post(url, files=files, headers=headers, data=parameters)

        job_id = response.text.replace('\"', '')

        req_url = url + str(job_id)

        headers = {
            "apikey": infrrd_api_key,
            "Content-Type": "application/x-www-form-urlencoded"
        }

        retry_counter = 0

        doc_data = {}

        while retry_counter < 5:
            print("Getting results from Infrrd url: {}".format(req_url))

            response = requests.get(
                url=req_url,
                headers=headers
            )

            if response.status_code != 404:

                print("Infrrd response: {}".format(response.status_code))

                doc_data = json.loads(response.text)
                job_status = doc_data['status']

                print("Infrrd job status: {}".format(job_status))

                if job_status == 'EXTRACTED':
                    try:
                        for field in doc_data['data']['fields']:
                            f = [dms_id, 'new_infrrd', "merchant_vat_number", doc_data['data']['fields'][field]['value'], doc_data['data']['fields'][field]['confidence']]
                            fields.append(f)
                    except KeyError:
                        print('No Infrrd vat number from new model')

                    break

                retry_counter += 1
                retry_timer = retry_counter * 2
                print("Retry infrrd fetch in : {}s".format(retry_timer))
                time.sleep(retry_timer)
            else:
                print("Infrrd job not found")
                break

        # document = {'document': file.content}
        # k_start = time.time()
        # klip_response = requests.post(url=klippa_url, files=document, headers=header)
        # k_end = time.time()
        # print('Done with klippa')
        #
        # try:
        #     klp_fields = json.loads(klip_response.content)['data']
        #
        #     # if execution gets to this point it is assumed the call succeeded, hence request time is recorded
        #     field = [dms_id, 'klippa', 'request_time', k_end - k_start, 0]
        #     fields.append(field)
        #
        #     for key in klp_fields.keys():
        #         if key in klippa_field_map.keys():
        #             field = [dms_id, 'klippa', klippa_field_map[key], klp_fields[key], 0]
        #             fields.append(field)
        #         elif key == "vatitems":
        #             vat_items = klp_fields[key]
        #             for item in vat_items:
        #                 for vat_key in item:
        #                     if vat_key in klippa_vat_item_field_map.keys():
        #                         field = [dms_id, 'klippa', klippa_vat_item_field_map[vat_key], item[vat_key], 0]
        #                         fields.append(field)
        #
        # except KeyError:
        #     print('Klippa call failed for some reason. ¯\_(ツ)_/¯')
        # except json.JSONDecodeError:
        #     print('Klippa call failed for some reason. ¯\_(ツ)_/¯')

        # Infrrd

        # print("Processing {}".format(dms_id))
        #
        # response = requests.request("POST", aikona_url, data=payload)
        #
        # print('Done with Aikona')
        #
        # try:
        #     results_list = json.loads(response.content)
        #
        #     for document in results_list:
        #         for extracted_field in document['extracted_fields']:
        #             if extracted_field['field_name'] in infrrd_field_map.keys():
        #                 field = [dms_id, 'infrrd', infrrd_field_map[extracted_field['field_name']],
        #                          extracted_field['data'], extracted_field['confidence_level']]
        #                 fields.append(field)
        #
        # except json.JSONDecodeError:
        #     print('Infrrd failed ')
        #
        df = pd.DataFrame(fields, columns=["DMS_ID", "Source", "Field_Name", "Field_Value", "Field_Confidence_Value"])
        # print(df)
        df.to_csv('new_infrrd_results.csv')

    except ConnectionError:
        print('Connection Error')

print('done')