# Aikona

A service that uses the OCR textract service to obtain the text from an image. Certain fields i.e. VAT numbers are then extracted from the OCR text.
## Prerequisites:

In order to get aikona running and building you'll need the following already installed on your system

 * docker

## Setup and Run

### Environment Variables
Example of environmental variables needed for the application. Note that some of the values are dummy values.

```bash
AWS_ACCESS_KEY_ID=ABCDEF123456
AWS_SECRET_ACCESS_KEY=ABCDEF123456
AWS_DEFAULT_REGION=eu-west-1
s3_bucket=techbranch-textract-ocr-test-uat
topic_arn=arn:aws:sns:us-east-1:475416570310:AmazonTextractOcrTopicUat
sqs_url=https://sqs.us-east-1.amazonaws.com/475416570310/ocr-textract-uat
role_arn=arn:aws:iam::475416570310:role/ocr-textract-role-uat
dms_url=http://dms-uat.vatit.tech/documents/1.0/document/
pg_database=aikona
pg_host=aikona-uat.cbstx4flp7pt.eu-west-1.rds.amazonaws.com
pg_password=ABCDEF123456
pg_user=postgres
pg_port=5432
INFRRD_INVOICE_UPLOAD_URL=https://eu.vision.infrrdapis.com/ocr/v3/invoice/
INFRRD_INVOICE_API_KEY=f0a6c420-e8c2-4ff1-ba2b-0f3f8d9dfa5f
INFRRD_RECEIPT_UPLOAD_URL=https://eu.vision.infrrdapis.com/ocr/v3/receipt/
INFRRD_RECEIPT_API_KEY=200178bc-acd3-4982-9d12-4273b78c307b
```

### Locally (without docker)

Run the following to install all necessary components (python3, pip):

```bash
RUN apt-get update -y
RUN apt-get install -y python-pip python
RUN pip install --upgrade pip==19.0.3 
```

Run the following to install all pip dependencies:

```bash
pip install -r requirements.txt 
```

To run the app:
```bash
/usr/local/bin/gunicorn src.main.app:app -b 0.0.0.0:5333 --timeout 3600 --graceful-timeout 3600 --log-level debug --keep-alive 3600
```

### Locally (with docker)

You also have the option of running the application locally in docker, in which case all you would need to do is build and run the docker image.

Build the image and tag it:

```bash
docker build -t aikona .
```

Then run the docker image:

```bash
docker run -p 5333:5333 aikona
```
### Infrrd API 

####Invoices
Fields returned
- remitToAddress (address)
- shipToAddress (address)
- currencyiso
- storeDetails (address)
- totalbillamount
- tax (amounts)
- purchaseorderno
- invoiceDate
- taxDetailList
- invoiceDateiso
- merchantname
- currency
- invoiceno
- category (expense category)

####Receipts
Fields returned
- currencyiso
- storeDetails
- totalbillamount
- vatPercent
- billingDate
- vatAmt
- merchantName
- currency
- category
- vatNumber