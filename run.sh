#!/usr/bin/env bash
docker stop aikona

docker rm aikona

docker build -t aikona:$1 .

docker run -d -p 80:5333 --name aikona --env-file ./envs/$1.env aikona:$1

#Crontab statement
#NB: Change env name depending on machine that the job will be run on
#10 4    * * *   root    cd /home/ubuntu/aikona && ./run.sh dev >> /home/ubuntu/logs/aikonacron.log 2>&1
